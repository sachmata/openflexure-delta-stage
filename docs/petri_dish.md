# Petri dish holder

This holds a 35mm Petri dish firmly on top of the stage for the OpenFlexure Delta Stage

{{BOM}}

[35mm petri dish holder]: models/35mm_petri_dish_holder.stl "{cat:3DPrinted}"
[M3x8mm screw]: "{cat:part}"


## Method

### Print the petri dish holder {pagestep}

Print the [35mm petri dish holder]{qty:1}.

### Attach the petri dish holder to the stage {pagestep}

Screw the [35mm petri dish holder] to the [main body](fromstep) using two [M3x8mm screw]{qty:2}s.  

![](images/petri_dish_holder/attach1.jpg)
![](images/petri_dish_holder/attach2.jpg)
![](images/petri_dish_holder/attach3.jpg)
![](images/petri_dish_holder/attach4.jpg)

### Push in the petri dish {pagestep}

The petri dish has a tight fit, and its lid can also be put on.

![](images/petri_dish_holder/petri1.jpg)
![](images/petri_dish_holder/petri2.jpg)
![](images/petri_dish_holder/petri3.jpg)