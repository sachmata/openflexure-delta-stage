# Star LED

You will require an LED Engin LZ1 LED with the Starboard.  You should choose which one you require based on the excitation wavelength.

eg. 
|Colour |Wavelength (nm)| Mouser Part No. |
|---|---|---|
|UV | 365 - 370 | [897-LZ1-10UV0R-0000](https://www.mouser.co.uk/ProductDetail/LED-Engin/LZ4-V0UB0R-00U4?qs=vmHwEFxEFR%252BXI1lg8CXaDA%3D%3D)
|Blue | 460 | [897-LZ110B2020000](https://www.mouser.co.uk/ProductDetail/LED-Engin/LZ1-10B202-0000?qs=QhAb4EtQfbWOLKWBru6FXA%3D%3D) |
|Green | 523 | [897-LZ110G102000](https://www.mouser.co.uk/ProductDetail/LED-Engin/LZ1-10G102-0000?qs=QhAb4EtQfbUZK37Yw6q95g%3D%3D) |
|Red | 623 | [897-LZ110R1020000](https://www.mouser.co.uk/ProductDetail/LED-Engin/LZ1-10R102-0000?qs=QhAb4EtQfbUOXmBwg3w3fQ%3D%3D) |