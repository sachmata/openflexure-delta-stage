# Delta Stage main body

[Delta Stage]: delta_stage.stl
[Delta Stage smart brim]: delta_stage_smart_brim.stl

This is just the stage, without the reflection illumination hole and transmission illumination condenser mount.

> **Warning:** You should not print the Delta Stage main body using a brim as it will interfere with the mechanism.  Instead use the smart brim version.

## Delta Stage

[Delta Stage]

## Delta Stage smart brim

[Delta Stage smart brim]