# Printing the components

These instructions show you how to print the parts and the printed tools for the OpenFlexure Delta Stage. You don't need to print all the files.  Follow the steps below to find the right files for your configuration.

We normally print with PLA using a 0.15 layer height and ~18% infill. You should not need to print any of the parts with adhesion settings (brim etc.) or supports.  Take note of the optics parts we recommend you print with black PLA, to reduce stray light.

[Delta Stage main body]: models/delta_stage_both.md "{cat:3DPrinted}"
[Delta Stage Microscope main body]: models/delta_stage_microscope_both.md "{cat: 3DPrinted}"
[feet]: models/feet.stl "{cat:3DPrinted, note: All three feet are in the one file.}"
[Raspberry Pi & Sangaboard base]: models/base_raspi_sangaboard.stl "{cat: 3DPrinted}"
[simple base]: models/base.stl "{cat: 3DPrinted}"
[band tool]: models/actuatortools.md#bandtool "{cat: 3DPrinted_tool}"
[nut tool]: models/actuatortools.md#nuttool "{cat:3DPrinted_tool}"
[lens tool]: models/lens_tool.stl "{cat:3DPrinted_tool}"
[sample clip]: models/sample_clips.stl "{cat: 3DPrinted, note: Both sample clips are in the one file.}"
[gear]: models/gears.stl "{cat: 3DPrinted, note: All three gears are in the one file.}"
[small gear]: models/small_gears.stl "{cat: 3DPrinted, note: All three gears are in the one file.}"
[illumination dovetail]: models/illumination_dovetail.stl "{cat:3DPrinted}"
[condenser housing]: models/condenser.stl "{cat:3DPrinted}"
[camera cover]: models/picamera_2_cover.stl "{cat:3DPrinted}"
[LED grid holder]: models/LED_array_holder.stl "{cat:3DPrinted}"
[transmission optics module casing]: models/optics_picamera2_rms_f50d13_delta.stl "{cat:3DPrinted}"
[reflection optics module casing]: models/optics_picamera2_rms_f50d13_beamsplitter_delta.stl "{cat:3DPrinted}"
[filter cube]: models/fl_cube.stl "{cat:3DPrinted}"
[reflection illumination holder]: models/reflection_illumination.md#holder "{cat:3DPrinted}"
[reflection illumination condenser]: models/reflection_illumination.md#condenser "{cat:3DPrinted}"
[35mm petri dish holder]: models/35mm_petri_dish_holder.stl "{cat:3DPrinted}"

## Main body

There are two options for the [main body]{output,qty:1}.

1. If you just want the stage, you will need to print the [Delta Stage main body].
2. If you want to use the Delta Stage Microscope, you will need to print [Delta Stage Microscope main body].

## Actuators

To control the movement of the stage, you will need to print:

1. The three [feet].
2. The three [gear]s.

## Printed tools

To assist with the fiddlier parts, you will need to print out these tools:

1. The [band tool]{qty:1}.
2. The [nut tool]{qty:1}.
3. The [lens tool]{qty:1}.

## Sample clips

To secure a microscope slide to the stage, you will need to print:

1. The two [sample clip]s.

## Motors

To connect the motors to the stage, you will need to print:

1. The three [small gear]s.

## Optics

### Transmission illumination

1. The [illumination dovetail].
2. The [condenser housing]. **Print in black**
3. The [transmission optics module casing]. **Print in black**
4. The [camera cover].

### Reflection illumination

1. The [filter cube]. **Print in black**
2. The [reflection illumination holder]. **Print in black**
3. The [reflection illumination condenser]. **Print in black**
4. The [reflection optics module casing]. **Print in black**
5. The [camera cover].

### LED grid for structured illumination

1. The [LED grid holder].

## Base

The base keeps the microscope steady. Choose which one you want to print:

1. The [simple base].

   or

2. The [Raspberry Pi & Sangaboard base].

### Stage adaptors

1. The [35mm petri dish holder].
